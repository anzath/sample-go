//go:build tools
// +build tools

/*
 * Copyright (C) 2021 by Enterprise Technology, Viet Thai International
 * All Rights Reserved.
 *
 * This source code is protected under international copyright law.  All rights
 * reserved and protected by the copyright holders.
 * This file is confidential and only available to authorized individuals with the
 * permission of the copyright holders.  If you encounter this file and do not have
 * permission, please contact the copyright holders and delete this file.
 */

package tools

import (
	_ "github.com/onsi/ginkgo/ginkgo"
	_ "github.com/onsi/gomega"
)

// This file imports packages that are used when running go generate, or used
// during the development process but not otherwise depended on by built code.
