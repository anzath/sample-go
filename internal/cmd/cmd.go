package cmd

import (
	"context"
	"os"

	"github.com/spf13/cobra"

	"gitlab.com/anzath/sample-go/internal/client/logger"
	"gitlab.com/anzath/sample-go/internal/config"
)

var rootCmd = &cobra.Command{
	Use:   "serve",
	Short: "Start the HTTP server and listen on port 8080.",
	Run: func(cmd *cobra.Command, args []string) {
		log := logger.CToL(context.Background(), "cmd.rootCmd.Run")

		if err := cmd.Help(); err != nil {
			log.WithError(err).Fatalf("cmd.Help returns error: %s", err.Error())
		}
		os.Exit(0)
	},
}

func init() {
	config.Load()

	rootCmd.AddCommand(serveCmd)
}

func Execute() {
	log := logger.CToL(context.Background(), "cmd.Execute")

	if err := rootCmd.Execute(); err != nil {
		log.WithError(err).Fatalf("rootCmd.Execute returns error: %s", err)
	}
}
