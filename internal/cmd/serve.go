package cmd

import (
	"context"

	"github.com/spf13/cobra"

	"gitlab.com/anzath/sample-go/internal/client/logger"
	"gitlab.com/anzath/sample-go/internal/http/handler"
	"gitlab.com/anzath/sample-go/internal/http/server"
)

var serveCmd = &cobra.Command{
	Use:   "serve",
	Short: "Start the HTTP server and listen on port 8080.",
	Run: func(cmd *cobra.Command, args []string) {
		log := logger.CToL(context.Background(), "cmd.serveCmd.Run")

		h := handler.New()
		s := server.New(h)

		if err := s.Run(); err != nil {
			log.WithError(err).Fatalf("s.Run returns error: %s", err)
		}
	},
}
