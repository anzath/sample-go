package config

import (
	"os"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("configImpl", func() {
	Describe("GetHTTPConfig", func() {
		Context("PORT is defined", func() {
			It("should returns", func() {
				os.Setenv("PORT", "8080")

				cfg := New().GetHTTPConfig()
				Expect(cfg.Port).To(BeEquivalentTo("8080"))
			})
		})

		Context("PORT is not defined", func() {
			It("should returns", func() {
				os.Setenv("PORT", "")

				cfg := New().GetHTTPConfig()
				Expect(cfg.Port).To(BeEquivalentTo("8080"))
			})
		})
	})

	Describe("GetLoggingConfig", func() {
		Context("production environment", func() {
			It("should returns", func() {
				os.Setenv("ENV", "prd")

				Load()

				v := New().GetLoggingConfig()
				Expect(v).To(BeEquivalentTo(LoggingConfig{
					Formatter: LoggingJSONFormatter,
					Level:     LoggingInfoLevel,
				}))
			})
		})

		Context("staging environment", func() {
			It("should returns", func() {
				os.Setenv("ENV", "stg")

				Load()

				v := New().GetLoggingConfig()
				Expect(v).To(BeEquivalentTo(LoggingConfig{
					Formatter: LoggingJSONFormatter,
					Level:     LoggingDebugLevel,
				}))
			})
		})

		Context("local environment", func() {
			It("should returns", func() {
				os.Setenv("ENV", "")

				Load()

				v := New().GetLoggingConfig()
				Expect(v).To(BeEquivalentTo(LoggingConfig{
					Formatter: LoggingTextFormatter,
					Level:     LoggingDebugLevel,
				}))
			})
		})
	})
})
