package config

import (
	"log"
	"os"

	"github.com/joho/godotenv"
	"github.com/spf13/viper"
)

func Load() {
	if _, err := os.Stat(".env"); err == nil {
		err := godotenv.Load()
		if err != nil {
			log.Fatal(err)
		}
	}

	viper.AutomaticEnv()
}

//go:generate mockery --name=Config --inpackage --case=snake
type Config interface {
	GetHTTPConfig() HTTPConfig
	GetLoggingConfig() LoggingConfig
}

func New() Config {
	return &configImpl{
		//
	}
}

type configImpl struct {
	//
}

type HTTPConfig struct {
	Port string
}

func (i *configImpl) GetHTTPConfig() HTTPConfig {
	viper.SetDefault("PORT", "8080")

	return HTTPConfig{
		Port: viper.GetString("PORT"),
	}
}

type LoggingConfig struct {
	Formatter LoggingFormatter
	Level     LoggingLevel
}

type LoggingFormatter string

var (
	LoggingJSONFormatter LoggingFormatter = "json"
	LoggingTextFormatter LoggingFormatter = "text"
)

type LoggingLevel string

var (
	LoggingInfoLevel  LoggingLevel = "info"
	LoggingDebugLevel LoggingLevel = "debug"
)

func (i *configImpl) GetLoggingConfig() LoggingConfig {
	env := viper.GetString("ENV")

	switch env {
	case "prd":
		return LoggingConfig{
			Formatter: LoggingJSONFormatter,
			Level:     LoggingInfoLevel,
		}

	case "stg":
		return LoggingConfig{
			Formatter: LoggingJSONFormatter,
			Level:     LoggingDebugLevel,
		}

	default:
		return LoggingConfig{
			Formatter: LoggingTextFormatter,
			Level:     LoggingDebugLevel,
		}
	}
}
