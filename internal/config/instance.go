package config

var instance Config

func Instance() Config {
	// This method provides a lightweight usage for downstream packages
	// which don't have a need of mocking configuration during the testing.

	if instance == nil {
		instance = New()
	}

	return instance
}
