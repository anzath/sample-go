package handler

import (
	"context"

	"gitlab.com/anzath/sample-go/dto"
)

//go:generate mockery --name=Handler --inpackage --case=snake
type Handler interface {
	Ping(context.Context) (*dto.PingResponse, error)
}

func New() Handler {
	return &handlerImpl{
		//
	}
}

type handlerImpl struct {
	//
}

func (i *handlerImpl) Ping(ctx context.Context) (*dto.PingResponse, error) {
	return &dto.PingResponse{
		Status: "OK",
	}, nil
}
