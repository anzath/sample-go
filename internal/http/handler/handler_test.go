package handler

import (
	"context"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("handlerImpl", func() {
	Describe("Ping", func() {
		Context("happy path", func() {
			It("should returns", func() {
				h := New()
				res, err := h.Ping(context.TODO())

				Expect(err).To(BeNil())
				Expect(res.Status).To(BeEquivalentTo("OK"))
			})
		})
	})
})
