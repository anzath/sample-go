package server

import (
	"context"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/anzath/sample-go/internal/client/logger"
	"gitlab.com/anzath/sample-go/internal/config"
	"gitlab.com/anzath/sample-go/internal/http/handler"
	"gitlab.com/anzath/sample-go/internal/http/middleware/loggingmiddleware"
)

func init() {
	gin.SetMode("release")
}

type Server interface {
	Run() error
}

func New(handler handler.Handler) Server {
	s := &serverImpl{
		handler: handler,
	}

	s.initServer()

	return s
}

type serverImpl struct {
	r *gin.Engine

	handler handler.Handler
}

func (i *serverImpl) Run() error {
	log := logger.CToL(context.Background(), "server.serverImpl.Run")

	cfg := config.Instance().GetHTTPConfig()
	addr := fmt.Sprintf(":%s", cfg.Port)

	log.Infof("HTTP server is listening at %s", addr)

	return i.r.Run(addr)
}

func (i *serverImpl) initServer() {
	r := gin.New()
	r.Use(gin.Recovery())
	r.Use(loggingmiddleware.LoggingMiddleware())

	r.GET("/", i.handleIndex)
	r.GET("/v1/ping", i.handlePing)

	i.r = r
}

func (i *serverImpl) handleIndex(c *gin.Context) {
	c.Status(200)
}

func (i *serverImpl) handlePing(c *gin.Context) {
	res, err := i.handler.Ping(c.Request.Context())
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	c.JSON(http.StatusOK, res)
}
