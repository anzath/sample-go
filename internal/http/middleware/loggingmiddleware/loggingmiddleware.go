package loggingmiddleware

import (
	"context"
	"fmt"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"

	"gitlab.com/anzath/sample-go/internal/client/logger"
)

func LoggingMiddleware() gin.HandlerFunc {
	log := logger.CToL(context.Background(), "loggingmiddleware")

	return func(c *gin.Context) {
		startTime := time.Now()

		c.Next()

		log = log.WithFields(logrus.Fields{
			"status":  c.Writer.Status(),
			"latency": time.Now().Sub(startTime),
			"method":  c.Request.Method,
			"url":     c.Request.URL.String(),
		})

		msg := fmt.Sprintf("%s %s", c.Request.Method, c.Request.URL.String())

		if c.Writer.Status() >= 200 && c.Writer.Status() < 400 {
			log.Info(msg)
		} else if c.Writer.Status() >= 400 && c.Writer.Status() < 500 {
			log.Warn(msg)
		} else {
			log.Error(msg)
		}
	}
}
