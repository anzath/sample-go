package logger

import (
	"context"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"github.com/sirupsen/logrus"
)

var _ = Describe("logger", func() {
	Describe("CToL", func() {
		It("should returns", func() {
			ctxWithoutLog := context.TODO()

			log := CToL(ctxWithoutLog, "test")
			Expect(log.Logger.Formatter).To(BeEquivalentTo(&logrus.TextFormatter{FieldMap: fieldMap}))

			log.Logger.SetFormatter(&logrus.JSONFormatter{})
			ctxWithLog := LToC(ctxWithoutLog, log)

			logFromContext := CToL(ctxWithLog, "test")
			Expect(logFromContext.Logger.Formatter).To(BeEquivalentTo(&logrus.JSONFormatter{}))
		})
	})
})
