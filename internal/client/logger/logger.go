package logger

import (
	"context"

	"github.com/sirupsen/logrus"

	"gitlab.com/anzath/sample-go/internal/config"
)

const (
	contextKey = "logger"
)

var (
	fieldMap = logrus.FieldMap{
		logrus.FieldKeyMsg: "message",
	}
)

func CToL(ctx context.Context, label string) *logrus.Entry {
	// CToL stands for Context-To-Log

	v := ctx.Value(contextKey)
	if v == nil {
		return initLog(label)
	}

	if log, ok := v.(*logrus.Entry); ok {
		log = log.WithField("label", label)
		return log
	}

	return initLog(label)
}

func LToC(parent context.Context, logger *logrus.Entry) context.Context {
	// LToC stands for Log-To-Context
	return context.WithValue(parent, contextKey, logger)
}

func initLog(label string) *logrus.Entry {
	cfg := config.Instance().GetLoggingConfig()

	log := logrus.New()

	switch cfg.Formatter {
	case config.LoggingJSONFormatter:
		log.SetFormatter(&logrus.JSONFormatter{FieldMap: fieldMap})
	case config.LoggingTextFormatter:
		log.SetFormatter(&logrus.TextFormatter{FieldMap: fieldMap})
	}

	switch cfg.Level {
	case config.LoggingInfoLevel:
		log.SetLevel(logrus.InfoLevel)
	case config.LoggingDebugLevel:
		log.SetLevel(logrus.DebugLevel)
	}

	return log.WithField("label", label)
}
