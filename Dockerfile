FROM golang:latest

WORKDIR /app

COPY go.mod go.sum ./

RUN go mod download

COPY . .

RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main .

EXPOSE 8080

FROM alpine:latest

WORKDIR /app

COPY --from=0 /app/main ./

CMD ["./main", "serve"]
